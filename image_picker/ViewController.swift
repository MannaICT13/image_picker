//
//  ViewController.swift
//  image_picker
//
//  Created by Md Khaled Hasan Manna on 27/11/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageViewOutlet: UIImageView!
    
    
    var imgPicker : UIImagePickerController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    
    @IBAction func addImgAction(_ sender: UIButton) {
        pickingImg()
        
        
        
    }
    

}

extension ViewController : UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func pickingImg(){
        
        imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.sourceType = .photoLibrary
        self.present(imgPicker, animated: true, completion: nil)
        
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let img = info[.originalImage] as! UIImage
        imageViewOutlet.image = img
        self.dismiss(animated: true, completion: nil)
        
        
        
        
    }
    
    
    
}

